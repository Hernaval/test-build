const svgLogo = document.getElementById('shape');
const logoLength = svgLogo.getTotalLength();

const svgLogo1 = document.getElementById('shape1');
const logoLength1 = svgLogo1.getTotalLength();

const svgLogo2 = document.getElementById('shape2');
const logoLength2 = svgLogo2.getTotalLength();

const svgLogo3 = document.getElementById('shape3');
const logoLength3 = svgLogo3.getTotalLength();

const svgLogo4 = document.getElementById('shape4');
const logoLength4 = svgLogo4.getTotalLength();

const svgLogo5 = document.getElementById('shape5');
const logoLength5 = svgLogo5.getTotalLength();

  svgLogo.style.strokeDasharray = logoLength;
  svgLogo.style.strokeDashOffset = logoLength;

  svgLogo1.style.strokeDasharray = logoLength1;
  svgLogo1.style.strokeDashOffset = logoLength1;

  svgLogo2.style.strokeDasharray = logoLength2;
  svgLogo2.style.strokeDashOffset = logoLength2;

  svgLogo3.style.strokeDasharray = logoLength3;
  svgLogo3.style.strokeDashOffset = logoLength3;

  svgLogo4.style.strokeDasharray = logoLength4;
  svgLogo4.style.strokeDashOffset = logoLength4;

  svgLogo5.style.strokeDasharray = logoLength5;
  svgLogo5.style.strokeDashOffset = logoLength5;




const drawWhenScroll = () => {
  document.getElementById('logo').style.opacity = 1;

  const drawLogo = logoLength * calcScrollPercent();

  const drawLogo1 = logoLength1 * calcScrollPercent();

  const drawLogo2 = logoLength2 * calcScrollPercent();

  const drawLogo3 = logoLength3 * calcScrollPercent();

  const drawLogo4 = logoLength4 * calcScrollPercent();

  const drawLogo5 = logoLength5 * calcScrollPercent();

  svgLogo.style.strokeDashoffset = logoLength - drawLogo;
  svgLogo1.style.strokeDashoffset = logoLength1 - drawLogo1;
  svgLogo2.style.strokeDashoffset = logoLength2 - drawLogo2;
  svgLogo3.style.strokeDashoffset = logoLength3 - drawLogo3;
  svgLogo4.style.strokeDashoffset = logoLength4 - drawLogo4;
  svgLogo5.style.strokeDashoffset = logoLength5 - drawLogo5;
  if (calcScrollPercent() === 1) {
    svgLogo.style.fill = 'rgba(255, 255, 255, 0.8)';
    svgLogo1.style.fill = 'rgba(255, 255, 255, 0.8)';
    svgLogo2.style.fill = 'rgba(255, 255, 255, 0.8)';
    svgLogo3.style.fill = 'rgba(255, 255, 255, 0.8)';
    svgLogo4.style.fill = 'rgba(255, 255, 255, 0.8)';
    svgLogo5.style.fill = 'rgba(255, 255, 255, 0.8)';
  } else {
    svgLogo.style.fill = 'transparent';
    svgLogo1.style.fill = 'transparent';
    svgLogo2.style.fill = 'transparent';
    svgLogo3.style.fill = 'transparent';
    svgLogo4.style.fill = 'transparent';
    svgLogo5.style.fill = 'transparent';
  }
}

const calcScrollPercent = () => {
  let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  return document.documentElement.scrollTop / height;
}

window.addEventListener('scroll', drawWhenScroll);
